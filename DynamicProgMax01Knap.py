#!/usr/bin/python
#note using python 3
import math
import ast
import time
class knapSackObj (object):
        def __init__(self, i, val, cost):
                self.array = []
                self.i = i
                self.val = val
                self.cost = cost
                self.array = [i, val, cost]
        #function to print all the contents of the array for use when we eventually return G

class holdMinAndTake (object):
        def __init__(self, minTab, takTab):
                self.minTab = minTab
                self.takTab = takTab

class holdSolAndOptVal (object):
        def __init__(self, solList, optVal):
                self.solList = solList
                self.optVal = optVal
                
def dynamicProgMax01Knap(i, val, cost, B):  
        #budCol = [0] * (B + 1)
        dynL = [[0 for x in range(B + 1)] for item in range(i + 1)]

        for budget in range(B + 1):
                dynL[0][budget] = 0
                
        for item in range(1, i + 1):
                for budget in range(0, B + 1):
                        
                        if (cost[item - 1] > budget):
                                dynL[item][budget] = dynL[item - 1][budget]
                        else:
                                dynL[item][budget] = max(dynL[item - 1][budget - cost[item - 1]] + val[item - 1],
                                                         dynL[item - 1][budget])
        return dynL[i][B]



def solveMaxKnapOpt(i, val, cost, t):
        indMaxVal = val.index(max(val))
        aMax = knapSackObj(indMaxVal, val[indMaxVal], cost[indMaxVal])
        minCost = [[0 for tCol in range ((i * aMax.val) + 1)] for item in range(i)]
        take = [[0 for takeCol in range ((i * aMax.val) + 1)] for item in range(i)]
        infin = math.inf

        for item in range (i):
                minCost[item][0] = 0
        for target in range (1, val[0] + 1): #start from 1 because first for covered target as 0
                minCost[0][target] = cost[0]
                take[0][target] = "Yes"
        for target in range (val[0] + 1, ((i * aMax.val) + 1)):
                minCost[0][target] = infin
                take[0][target] = "No"
        for item in range (1, i):
                for target in range((i * aMax.val) + 1):
                        nextT = max(0, (target - val[item]))
                        if (minCost[item - 1][target] <= cost[item] + minCost[item - 1][nextT]):
                                minCost[item][target] = minCost[item - 1][target]
                                take[item][target] = "No"
                        else:
                                minCost[item][target] = cost[item] + minCost[item - 1][nextT]
                                take[item][target] = "Yes"
        returnMinAndTake = holdMinAndTake(minCost, take)                               
        return returnMinAndTake

def constructMaxKnapSol(i, val, cost, B, minCost, take):
        indMaxVal = val.index(max(val))
        aMax = knapSackObj(indMaxVal, val[indMaxVal], cost[indMaxVal])
        optimalValue = i * aMax.val
        #print (optimalValue)
        while (optimalValue > 0 and minCost[i - 1][optimalValue] > B):
                optimalValue -= 1
        solution = []
        x = i
        t = optimalValue
        #print (t)
        while (x > 0 and t > 0):
                if (take[x - 1][t - 1] == "Yes"):
                        solution.append(x - 1) #indice of the item taken for the solution 
                        t = t - val[x - 1]
                x -= 1
        returnSolAndOpt = holdSolAndOptVal(solution, optimalValue)
        return returnSolAndOpt.optVal #switch to solution to get the solution list

def knapApproxSch (i, val, cost, t, B, F):
        scaledVal = [0 for space in range(i)]
        for item in range(i):
                scaledVal[item] = math.floor((val[item]/F))
        holderObj = solveMaxKnapOpt(i, scaledVal, cost, t)
        minTable = holderObj.minTab
        takeTable = holderObj.takTab
        result = constructMaxKnapSol(i, scaledVal, cost, B, minTable, takeTable)
        return result
                
def greedyTwoApproxKnap(i, val, cost, B):
        #need to preprocess lists for costs over the balance
        G = []
        sortedList = []
        indices = []
        L = B
        gVals = []
        
        for y in range(0, i):
                
                sortedList.append(val[y]/cost[y])
                indices.append(y)
                
        #print (sortedList)
        #print (indices)
        
        sortInd = [indices for (sortedList, indices) in sorted(zip(sortedList,indices), reverse=True)]
        sortedList.sort(reverse=True)
        #print (sortInd)
        for i in sortInd:
                if (L > 0):
                        if (cost[i] <= L):
                                G.append(knapSackObj(i, val[i], cost[i]))
                                gVals.append(val[i])
                                L -= cost[i]
                else:
                        pass
        
        iMax = max(val)
        if ( iMax > sum(gVals)):
                return iMax
        else:
                return sum(gVals)
dynProgMax01KnapAnsList = []
greTwoApproxKnapAnsList = []
solveMaxKnapAnsList = []
knapApproxFIsFive = []

dynProgMax01KnapTimeList = []
greTwoApproxKnapTimeList = []
solveMaxKnapTimeList = []
knapApproxFIsFiveTimeList = []
averageAns = []
F = open("testValsKnap.txt", "r")
for line in F:
        i = int(F.readline())
#print (i)
        valList = ast.literal_eval(F.readline())
#print (valList)
        costList = ast.literal_eval(F.readline())
#print (costList)
        B = int(F.readline())
#print (B)
        start = time.time()
        dynProgMax01KnapAnsList.append(dynamicProgMax01Knap(i, valList, costList, B))
        stop = time.time()
        dynProgMax01KnapTimeList.append(stop - start)

        start = time.time()
        greTwoApproxKnapAnsList.append(greedyTwoApproxKnap(i, valList, costList, B))
        stop = time.time()
        greTwoApproxKnapTimeList.append(stop - start)

        start = time.time()
        tabHolder = solveMaxKnapOpt(i, valList, costList, B)
        minTable = tabHolder.minTab
        takeTable = tabHolder.takTab
        solveMaxKnapAnsList.append(constructMaxKnapSol(i, valList, costList, B, minTable, takeTable))
        stop = time.time()
        solveMaxKnapTimeList.append(stop - start)

        start = time.time()
        knapApproxFIsFive.append(knapApproxSch(i, valList, costList, B, B, 5))
        stop = time.time()
        knapApproxFIsFiveTimeList.append(stop - start)
        averageAns.append(((dynProgMax01KnapAnsList[0] + greTwoApproxKnapAnsList[0] + solveMaxKnapAnsList[0] + knapApproxFIsFive[0])/4))
        
